from app import app
from flask import render_template, flash, redirect, url_for
from app.forms import LoginForm
from app.models import User
from flask_login import current_user, login_user, logout_user

@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'Grzesiek11'}
    posts = [
        {
            'username': 'TadeLn',
            'body': 'KMM10 - nowy świat 62-8! Course ID: 4H4T-0N13-ZL30'
        },
        {
            'username': 'Selcior',
            'body': 'TempleOS właśnie został najpopularniejszym systemem świata, h3h3! I co wy Linuksiarze, dalej 20%?'
        },
        {
            'username': 'BotWnioskowy',
            'body': 'I\'m back and better than ever!'
        },
        {
            'username': 'FitMC',
            'body': 'ThE oLdEsT aNaRcHy SeRvEr In MiNeCrAfT!'
        }
    ]
    return render_template('index.html', user = user, posts = posts)

@app.route('/login', methods = ['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.objects(username = form.username.data).first()
        if user is None or not user.checkPassword(form.password.data):
            flash('Invalid username or password.')
            return redirect(url_for('login'))
        login_user(user, remember = form.rememberMe.data)
        return redirect(url_for('index'))
    return render_template('login.html', title = 'Sign in', form = form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))